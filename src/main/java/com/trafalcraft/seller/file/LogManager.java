package com.trafalcraft.seller.file;

import com.trafalcraft.seller.Main;

import java.io.IOException;
import java.util.logging.*;

public class LogManager {

    private LogManager() {}

    private static Logger logger;
    private static FileHandler fh;


    public static void write(String output) {
        if (logger == null) {
            logger = Logger.getLogger("TransactionLog");
            try {

                // This block configure the logger with handler and formatter
                int amountOfLogFiles = Main.getPlugin().getConfig().getInt("Settings.logs.amountOfLogFiles");
                int sizeOfLogFilesInMB = Main.getPlugin().getConfig().getInt("Settings.logs.sizeOfLogFilesInMB");
                fh = new FileHandler(Main.getPlugin().getDataFolder().getPath() + "//transaction-%g.log", sizeOfLogFilesInMB * 1000, amountOfLogFiles, true);
                logger.addHandler(fh);
                LogFormatter formatter = new LogFormatter();
                fh.setFormatter(formatter);
                logger.setUseParentHandlers(false);
            } catch (SecurityException e) {
                Main.getPlugin().getLogger().severe("The plugin dont have the right to write log file in his own directory : " + Main.getPlugin().getDataFolder().getPath());                
            } catch (IOException e) {
                Main.getPlugin().getLogger().severe("Error when trying to write logging file in : " + Main.getPlugin().getDataFolder().getPath() + " : " + e.getMessage());
            }
        }

        logger.info(output);
    }

    public static void close() {
        if (logger != null) {
            fh.close();
        }
    }
}
