package com.trafalcraft.seller.file;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {

    public String format(LogRecord record) {
        StringBuffer s = new StringBuffer(1000);
        Date d = new Date(record.getMillis());
        DateFormat df = DateFormat.getDateTimeInstance(
                DateFormat.LONG, DateFormat.MEDIUM, Locale.ENGLISH);
        s.append(df.format(d) + " : "+formatMessage(record)+ "\n");
        return s.toString();
    }
    // début du fichier de log
    public String getHead(Handler h) {
        return "Transaction log for tr-Seller\n";
    }
}
