package com.trafalcraft.seller.setup;

import com.trafalcraft.seller.Main;
import com.trafalcraft.seller.util.Msg;
import com.trafalcraft.seller.util.SpecialChar;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;

public class SetupInventoryManage {
    private SetupInventoryManage() {}

    public static void openManageInventory(Player p, String type, int pageNumber) {
        Inventory inventory = Bukkit
                .createInventory(p, 54,
                        SpecialChar.SELLER_INVENTORY_HEADER + "§4Manage>" + Msg.SELLER_NAME
                                + "§4" + SpecialChar.SELLER_INVENTORY_SEPARATOR + "§4"
                                + type + "§4" + SpecialChar.SELLER_INVENTORY_SEPARATOR + "§4" + pageNumber);
        File file = new File(Main.getPlugin().getDataFolder() + "//shops//", type + ".yml");
        YamlConfiguration yc;
        if (file.exists()) {
            yc = YamlConfiguration.loadConfiguration(file);
            for (int i = 0; i < 45; i++) {
                Object itemTest = yc.get("page." + pageNumber + ".item." + i + ".itemStack");
                if (itemTest instanceof ItemStack) {
                    ItemStack item = (ItemStack) itemTest;
                    inventory.setItem(i, item);
                }
            }
        }
        if (pageNumber != 1) {
            ItemStack oldPage = new ItemStack(Material.ARROW);
            ItemMeta oldPageMeta = oldPage.getItemMeta();
            oldPageMeta.setDisplayName(Msg.PREVIOUS_PAGE_OF_SHOP.toString());
            oldPage.setItemMeta(oldPageMeta);
            inventory.setItem(47, oldPage);
        }
        ItemStack save;
        try {
            save = new ItemStack(Material.GREEN_TERRACOTTA);
        } catch (NoSuchFieldError e) {
            save = new ItemStack(Material.getMaterial("STAINED_CLAY"));
            ItemMeta meta = save.getItemMeta();
            if (meta instanceof Damageable) {
                ((Damageable) meta).setDamage((short) 13);
                save.setItemMeta(meta);
            } else if ((meta instanceof Damageable)) {
                Main.getPlugin().getLogger().warning("Stained clay is not damageable");
            }
        }
        ItemMeta saveMeta = save.getItemMeta();
        saveMeta.setDisplayName(Msg.SAVE_SHOP.toString());
        save.setItemMeta(saveMeta);
        inventory.setItem(48, save);
        ItemStack close;
        try {
            close = new ItemStack(Material.RED_TERRACOTTA);
        } catch (NoSuchFieldError e) {
            close = new ItemStack(Material.getMaterial("STAINED_CLAY"));
            ItemMeta meta = close.getItemMeta();
            if (meta instanceof Damageable) {
                ((Damageable) meta).setDamage((short) 14);
                close.setItemMeta(meta);
            } else if ((meta instanceof Damageable)) {
                Main.getPlugin().getLogger().warning("Stained clay is not damageable");
            }
        }
        ItemMeta closeMeta = close.getItemMeta();
        closeMeta.setDisplayName(Msg.CANCEL_SHOP.toString());
        close.setItemMeta(closeMeta);
        inventory.setItem(50, close);
        ItemStack nextPage = new ItemStack(Material.ARROW);
        ItemMeta nextPageMeta = nextPage.getItemMeta();
        nextPageMeta.setDisplayName(Msg.NEXT_PAGE_OF_SHOP.toString());
        nextPage.setItemMeta(nextPageMeta);
        inventory.setItem(51, nextPage);
        p.openInventory(inventory);
    }
}
