package com.trafalcraft.seller.util;

public enum SpecialChar {
    SELLER_INVENTORY_HEADER('\u21B3'),
    SELLER_INVENTORY_SEPARATOR('\u2503');

    private String value;

    SpecialChar(char value) {
        this.value = new String(new char[]{value});
    }

    @Override
    public String toString() {
        return value;
    }
}
