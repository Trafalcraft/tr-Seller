package com.trafalcraft.seller.api;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Object to manage transaction information
 *
 * @author Amosar
 */
public class TransactionData {
    private final Player player;
    private final String shopType;
    private int price;
    private final int pageNumber;
    private final int slotNumber;
    private ItemStack item;
    private final String transactionType;
    private boolean cancelTransaction = false;
    private String cancelReason;
    private boolean forceTransaction = false;

    /**
     * Constructs a TransactionData object with specified information
     *
     * @param player          The player object.
     * @param shopType        The type of the shop (corresponding to the config file name and sellerName).
     * @param price           The price of the item.
     * @param pageNumber      The page number of the item into the shop.
     * @param slotNumber      The slot number of the item into the page.
     * @param item            The item with his Data
     * @param transactionType The type of the transaction (sell or buy)
     */
    public TransactionData(Player player, String shopType, int price, int pageNumber
            , int slotNumber, ItemStack item, String transactionType) {
        this.player = player;
        this.shopType = shopType;
        this.price = price;
        this.pageNumber = pageNumber;
        this.slotNumber = slotNumber;
        this.item = item;
        this.transactionType = transactionType;
    }

    /**
     * Return the player who do the transaction
     *
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Return the type of the shop (corresponding to the config file name and sellerName)
     *
     * @return the type of the shop
     */
    public String getShopType() {
        return shopType;
    }

    /**
     * Return the price of the item
     *
     * @return the price of the item
     */
    public int getPrice() {
        return price;
    }

    /**
     * Set the price of the item
     *
     * @param price price of the item
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Return the page number of the item into the shop.
     *
     * @return The page number (-1 if transactionType is sell)
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Return the slot number of the item into the page.
     *
     * @return The slot number
     */
    public int getSlotNumber() {
        return slotNumber;
    }

    /**
     * Return the ItemStack of the transaction.
     *
     * @return the ItemStack
     */
    public ItemStack getItem() {
        return item;
    }

    /**
     * Replace the base itemStack of the transaction.
     *
     * @param item the new ItemStack
     */
    public void setItem(ItemStack item) {
        this.item = item;
    }

    /**
     * Return a String corresponding to the type of the transaction (buy or sell)
     *
     * @return the type of the transaction
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * cancel the transaction (Override the {@link #forceTransaction} method)
     */
    public void cancelTransaction() {
        cancelTransaction = true;
    }

    /**
     * cancel the transaction with a reason (Override the {@link #forceTransaction} method)
     *
     * @param reason Give the reason to cancel the transaction.
     */
    public void cancelTransaction(String reason) {
        cancelReason = reason;
        cancelTransaction = true;
    }

    /**
     * Get the cancellation state of the transaction
     *
     * @return boolean cancellation state
     */
    public boolean isCancelTransaction() {
        return cancelTransaction;
    }

    /**
     * Return the reason why the transaction is canceled
     *
     * @return The reason why the transaction is canceled
     */
    public String getCancelReason() {
        return cancelReason;
    }

    /**
     * Force the transaction to be allowed no matter if the user don't have the money
     * (The method is override by the {@link #cancelTransaction} method)
     */
    public void forceTransaction() {
        forceTransaction = true;
    }

    /**
     * Get the forceTransaction state of the transaction
     *
     * @return boolean forceTransaction state
     */
    public boolean isForcedTransaction() {
        return forceTransaction;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("cancelReason=" + cancelReason);
        toString.append(", cancelTransaction=" + cancelTransaction);
        toString.append(", forceTransaction=" + forceTransaction);
        toString.append(", item=" + item.getType() + 'x' + item.getAmount());
        if (!item.getEnchantments().isEmpty()) {
            toString.append(">{Enchantments=" + item.getEnchantments()+"}");
        }
        if (item.getItemMeta().hasDisplayName()) {
            toString.append(">{DisplayName=" + item.getItemMeta().getDisplayName()+"}");
        }
        toString.append(", pageNumber=" + pageNumber);
        toString.append(", player=" + player.getDisplayName());
        toString.append(", price=" + price);
        toString.append(", shopType=" + shopType);
        toString.append(", slotNumber=" + slotNumber);
        toString.append(", transactionType=" + transactionType);

        return "cancelReason=" + cancelReason 
                + ", cancelTransaction=" + cancelTransaction
                + ", forceTransaction=" + forceTransaction
                + ", item=" + item.getType() + 'x' + item.getAmount() +":"
                    + " {Enchantments=" + item.getEnchantments()+", DisplayName="+item.getItemMeta().getDisplayName()+"}"
                + ", pageNumber=" + pageNumber
                + ", player=" + player.getDisplayName()
                + ", price=" + price
                + ", shopType=" + shopType
                + ", slotNumber=" + slotNumber
                + ", transactionType=" + transactionType;
    }

    
}
