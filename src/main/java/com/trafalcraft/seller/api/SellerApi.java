package com.trafalcraft.seller.api;

import java.util.HashMap;

import org.bukkit.Bukkit;

/**
 * Register your plugin to link your plugin with the SellerApi
 *
 * @author Amosar
 */
public class SellerApi {
    private SellerApi() {}

    private static final HashMap<String, SellerPlugin> pluginList = new HashMap<>();

    /**
     * Register your plugin
     *
     * @param plugin The class that implement SellerPlugin
     */
    public static void registerPlugin(String nomPlugin, SellerPlugin plugin) {
        pluginList.put(nomPlugin, plugin);
    }

    /**
     * Unregister your plugin
     *
     * @param nomPlugin The name of your plugin
     */
    public static void unregisterPlugin(String nomPlugin) {
        SellerPlugin remove = pluginList.remove(nomPlugin);
        if (remove == null) {
            Bukkit.getLogger().warning("tr-SellerApi> error the plugin has not been registered to the plugin");
        }
    }

    public static SellerPlugin[] getAllPlugins() {
        return pluginList.values().toArray(new SellerPlugin[]{});
    }

}
