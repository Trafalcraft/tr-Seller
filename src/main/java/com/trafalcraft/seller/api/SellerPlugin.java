package com.trafalcraft.seller.api;

import org.bukkit.entity.Player;

/**
 * Implement this class in your plugin to link your plugin with the SellerApi
 *
 * @author Amosar
 */
public class SellerPlugin implements SellerPluginInternal {

    /**
     * Check is player can open shop.
     * If one plugin not allow the shop access it override all other plugins.
     *
     * @param player   The player that want to access to a shop
     * @param shopName The name of the shop
     * @return true if the player can access to the shop. false if it can't.
     */
    @Override
    public boolean allowShopAccess(Player player, String shopName) {
        return true;
    }

    /**
     * Check if player can buy or sell an item.
     * If one plugin doesn't allow the transaction, it override all other plugins.
     *
     * @param transactionData object that contain TransactionData information
     * @return return the transactionData object with your modifications.
     */
    @Override
    public TransactionData allowTransaction(TransactionData transactionData) {
        return transactionData;
    }

    /**
     * Get if the transaction is successful or not
     *
     * @param success         true if the transaction is a success.
     * @param transactionData object that contain TransactionData information.
     * @param errorMessage    Explain why the transaction is a failure.
     */
    @Override
    public void getTransactionResult(boolean success, TransactionData transactionData, String errorMessage) {

    }
}
