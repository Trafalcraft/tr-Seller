package com.trafalcraft.seller.api;

import org.bukkit.entity.Player;

interface SellerPluginInternal {
    boolean allowShopAccess(Player player, String shopName);

    TransactionData allowTransaction(TransactionData transactionData);

    void getTransactionResult(boolean success, TransactionData transactionData, String reason);

}
