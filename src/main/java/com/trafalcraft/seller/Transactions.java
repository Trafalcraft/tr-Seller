package com.trafalcraft.seller;

import com.trafalcraft.seller.api.SellerApi;
import com.trafalcraft.seller.api.SellerPlugin;
import com.trafalcraft.seller.api.TransactionData;
import com.trafalcraft.seller.file.LogManager;
import com.trafalcraft.seller.file.ShopManager;
import com.trafalcraft.seller.util.Msg;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

class Transactions {

    public void buy(Player p, String type, int pageNumber, int slot, int amount) {
        YamlConfiguration yamlShop = ShopManager.getShop(type);
        int buyPrice = yamlShop.getInt("page." + pageNumber + ".item." + slot + ".buy") * amount;
        ItemStack item = (ItemStack) yamlShop.get("page." + pageNumber + ".item." + slot + ".itemStack");
        TransactionData transactionData = new TransactionData(p, type, buyPrice, pageNumber
                , slot, item, "buy");
        for (SellerPlugin sellerPlugin : SellerApi.getAllPlugins()) {
            transactionData = sellerPlugin.allowTransaction(transactionData);
            buyPrice = transactionData.getPrice();
            item = transactionData.getItem();
        }


        if (transactionData.isCancelTransaction()) {
            p.sendMessage(transactionData.getCancelReason());
            sendTransactionResult(false, transactionData, transactionData.getCancelReason());
        } else {
            if (buyPrice == 0 && !transactionData.isForcedTransaction()) {
                p.sendMessage(Msg.BUY_UNAVAILABLE.toString());
                sendTransactionResult(false, transactionData, Msg.BUY_UNAVAILABLE.toString());
            } else {
                String itemName;
                if (item.getItemMeta().getDisplayName() != null && !item.getItemMeta().getDisplayName().isEmpty()) {
                    itemName = item.getItemMeta().getDisplayName();
                } else {
                    itemName = WordUtils.capitalizeFully(item.getType().name().replace("_", ""));
                }
                double playerBalance = Main.getEcon().getBalance(p.getPlayer());
                if (playerBalance >= buyPrice || transactionData.isForcedTransaction()) {
                    Main.getEcon().withdrawPlayer(p, buyPrice);

                    p.sendMessage(Msg.SUCCESS_BUY.toString()
                            .replace("$moneySpent", Main.getEcon().format(buyPrice) + "")
                            .replace("$amountItem", amount + "")
                            .replace("$itemType", itemName)
                            .replace("$currentMoney", Main.getEcon()
                                    .format(Main.getEcon().getBalance(p.getPlayer()))));
                    for (int i1 = 0; i1 < amount; i1++) {
                        p.getInventory().addItem(item);
                    }
                    sendTransactionResult(true, transactionData, null);


                } else {
                    String msg = Msg.FAILURE_BUY_NO_ENOUGH_MONEY.toString()
                            .replace("$moneyMissing",
                                    "" + Main.getEcon().format(buyPrice - playerBalance))
                            .replace("$itemType", itemName)
                            .replace("$amount", amount + "");
                    p.sendMessage(msg);
                    sendTransactionResult(false, transactionData, msg);

                }
            }
        }
    }

    public void sell(Player p, ItemStack item, String type, int slot) {
        YamlConfiguration yamlShop = ShopManager.getShop(type);
        for (String page : yamlShop.getConfigurationSection("page").getKeys(false)) {
            for (String itemIndex : yamlShop
                    .getConfigurationSection("page." + page + ".item").getKeys(false)) {
                ItemStack shopItem = (ItemStack) yamlShop
                        .get("page." + page + ".item." + itemIndex + ".itemStack");
                item.getItemMeta().setDisplayName(null);
                ItemStack tempItem = item.clone();
                tempItem.setAmount(1);
                if (tempItem.equals(shopItem)) {
                    int sellPrice = yamlShop
                            .getInt("page." + page + ".item." + itemIndex + ".sell");
                    TransactionData transactionData = new TransactionData(p, type, sellPrice, -1
                            , slot, item, "sell");
                    for (SellerPlugin sellerPlugin : SellerApi.getAllPlugins()) {
                        transactionData = sellerPlugin.allowTransaction(transactionData);
                        sellPrice = transactionData.getPrice();
                        item = transactionData.getItem();
                    }
                    if (transactionData.isCancelTransaction()) {
                        p.sendMessage(transactionData.getCancelReason());
                        sendTransactionResult(false, transactionData, transactionData.getCancelReason());
                        return;
                    } else {
                        String itemName;
                        if (shopItem.getItemMeta().getDisplayName() != null && !item.getItemMeta().getDisplayName().isEmpty()) {
                            itemName = shopItem.getItemMeta().getDisplayName();
                        } else {
                            itemName = WordUtils
                                    .capitalizeFully(shopItem.getType().name().replace("_", ""));
                        }
                        if (sellPrice != 0 || transactionData.isForcedTransaction()) {
                            Main.getEcon().depositPlayer(p, sellPrice * item.getAmount());
                            p.sendMessage(Msg.SUCCESS_SELL.toString()
                                    .replace("$moneySpent",
                                            Main.getEcon()
                                                    .format(sellPrice * item.getAmount())
                                                    + "")
                                    .replace("$amountItem", item.getAmount() + "")
                                    .replace("$itemType", itemName)
                                    .replace("$currentMoney",
                                            Main.getEcon().format(Main.getEcon()
                                                    .getBalance(p.getPlayer()))));
                            if (slot >= 81 && slot <= 89) {
                                int slot2 = slot - 81;
                                p.getInventory()
                                        .setItem(slot2, new ItemStack(Material.AIR));
                            }
                            int sellerInventorySizeWithItems = 44;
                            if (slot <= 80 && slot >= sellerInventorySizeWithItems + 1) {
                                int slot2 = slot - sellerInventorySizeWithItems - 1;
                                p.getInventory()
                                        .setItem(slot2, new ItemStack(Material.AIR));
                            }
                            sendTransactionResult(true, transactionData, null);
                            return;

                        }
                    }
                }
            }
        }
        TransactionData transactionData = new TransactionData(p, type, -1, -1
                , slot, item, "sell");
        sendTransactionResult(false, transactionData, Msg.FAILURE_SELL_NO_ITEM.toString());
        p.sendMessage(Msg.FAILURE_SELL_NO_ITEM.toString());

    }

    private void sendTransactionResult(boolean success, TransactionData transactionData, String errorMsg) {
        for (SellerPlugin sellerPlugin : SellerApi.getAllPlugins()) {
            sellerPlugin.getTransactionResult(success, transactionData, errorMsg);
        }
        if (Main.getPlugin().getConfig().getBoolean("Settings.log_transactions")) {
            String output = "Success=" + success + " <>DATA=" + transactionData + " <>errorMSG=" + errorMsg;
            LogManager.write(output);
        }
    }
}
